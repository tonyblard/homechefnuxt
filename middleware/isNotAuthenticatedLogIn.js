
export default async function ({ store, redirect, $axios }) {

  if (store.state.jwt) {
    let user = []
    let response = null
    await $axios.get('/user/me').then(response => (user = response.data))
    if (user.chef_id !== null) {
      return redirect('/chef/profil')
    }else {
      return redirect('/homeChef/menus')
      //user/reservervation
    }

  }
}
